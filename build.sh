#! /usr/bin/env bash

[[ ${#} -ne 1 ]] && { echo "You must provide a Dockerfile as the only parameter!"; exit -1; }
# You need to create a docker buildx multiarch builder first:
# docker buildx use $(docker buildx create --name multiarch-builder --driver docker-container --platform linux/amd64,linux/arm64,linux/arm/v5)

# Then build the multiarch image and push it directly to the repo.
(
archs=${ARCHS:="linux/amd64,linux/arm64"}
d=${1}
t=${TAG:=$(date -u +'%F-%H.%M.%S')}
tag=${REGISTRY:=registry.gitlab.com/tjuerges/docker}/${1/Dockerfile./}:${t}
docker buildx build --platform ${archs} --load -t ${1/Dockerfile./}:${t} -t ${tag} -f ${d} .
)
