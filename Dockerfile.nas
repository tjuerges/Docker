ARG VERSION=trixie-slim
FROM --platform=linux/amd64 debian:${VERSION}
ARG VERSION=trixie-slim

RUN apt-get update -q && \
    apt-get upgrade -y -q && \
    apt-get install --no-install-recommends --no-install-suggests -y -q \
        aptitude apt-file sudo adduser \
        ca-certificates curl vim mosh most mc htop tmux xterm \
        iftop iproute2 net-tools netcat-openbsd rsync tcpdump telnet traceroute \
        python3 python3-pip python3-venv \
        build-essential git cmake pkg-config gdb libtool autoconf automake

RUN useradd -m -s /bin/bash thomas && \
    adduser thomas sudo && \
    echo "thomas:foo" | chpasswd && \
    echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers && \
    printf "X11UseLocalhost no\n" >> /etc/ssh/sshd_config && \
    mkdir -p /run/sshd

EXPOSE 22
SHELL ["/bin/bash", "-c"]
ENV VERSION=${VERSION}
ENTRYPOINT ["/bin/bash"]
USER thomas
WORKDIR /home/thomas
